package com.alexglazovsky.atmosphereatm;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class Main extends TabActivity {

	/*private GeoPoint getPoint(double lat, double lon) {
		return (new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6)));
	}
*/
	
	//TabSpec names
	
	private static final String GOOGLE_MAPS_SPEC = "Google";
	private static final String YANDEX_MAPS_SPEC = "Yandex";
	private static final String TASKS_SPEC = "Tasks";
	
	TabHost tabhost;
        
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		tabhost = getTabHost();
		
	/*	tabs = (TabHost) findViewById(R.id.tabhost);
		tabs.setup();*/
		
//		Google Maps Tab
        TabSpec googleMapsSpec = tabhost.newTabSpec(GOOGLE_MAPS_SPEC);
//       Set title
        googleMapsSpec.setIndicator(GOOGLE_MAPS_SPEC);
        Intent googleMapsIntent = new Intent(this, GoogleMapsActivity.class);
//        Tab Content
        googleMapsSpec.setContent(googleMapsIntent);
        
        
//		Yandex Maps Tab
        TabSpec yandexMapsSpec = tabhost.newTabSpec(YANDEX_MAPS_SPEC);
        yandexMapsSpec.setIndicator(YANDEX_MAPS_SPEC);
        Intent yandexMapsIntent = new Intent(this, YandexMapsActivity.class);
        yandexMapsSpec.setContent(yandexMapsIntent);
		
        
//		Tasks Tab
        TabSpec tasksSpec = tabhost.newTabSpec(TASKS_SPEC);
        tasksSpec.setIndicator(TASKS_SPEC);
        Intent tasksIntent = new Intent(this, TasksActivity.class);
        tasksSpec.setContent(tasksIntent);
        
     
//		Adding all TabSpec to TabHost
        tabhost.addTab(googleMapsSpec);
        tabhost.addTab(yandexMapsSpec);
        tabhost.addTab(tasksSpec);
		
	}
	
//	��������� ���� �� xml
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
			menuInflater.inflate(R.menu.menu, menu);
//			return super.onCreateOptionsMenu(menu);
		
		return true;
	}
	
//	������ �������� Tabbar
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.chekbox:
			if (item.isChecked()) {
				item.setChecked(false);
//				Log.d("Alex", "unchecked");
				tabhost.getTabWidget().setVisibility(View.GONE);
			} else {
				item.setChecked(true);
//				Log.d("Alex", "checked");
				tabhost.getTabWidget().setVisibility(View.VISIBLE);
			}
			
		}
			return true;
//		return super.onOptionsItemSelected(item);
	}
	
	
}