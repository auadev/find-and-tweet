package com.alexglazovsky.atmosphereatm;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class GoogleMapsActivity extends MapActivity {

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_maps);
        
        final MapView mv = (MapView) findViewById(R.id.themap);
		mv.setBuiltInZoomControls(true);
		
		final MapController mapController = mv.getController();
		
		final List<Overlay> mapOverlays = mv.getOverlays();
		final Drawable drawable = this.getResources().getDrawable( R.drawable.androidmarker);
		final HelloItemizedOverlay itemizedoverlay = new HelloItemizedOverlay( drawable, this);
		
		final LocationManager locationMngr = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);

		if (!locationMngr.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//			createGpsDisabledAlert() {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage("Your GPS is disabled! Please enable it").setCancelable(false).setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent gpsOptionsIntent = new Intent(  
				                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);  
				        startActivity(gpsOptionsIntent);  
					}
				});
				AlertDialog alertDialog = builder.create();
				alertDialog.show();
//			}
		}
	

		final LocationListener listener = new LocationListener() {

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				// TODO Auto-generated method stub
				Log.d("provider", "status changed");
			}

			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				Log.d("provider", "enabled");
			}

			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
				Log.d("provider", "disabled");
			}

			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				/*
				 * Log.d("longtitude",
				 * Double.toString(location.getLongitude())); Log.d("latitude",
				 * Double.toString(location.getLatitude()));
				 */

				itemizedoverlay.clear();
				mapOverlays.clear(); // �������� ������� ������
				mv.invalidate();
			}
		
		};
		
		locationMngr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
				10, listener);
		final Button find = (Button) findViewById(R.id.find);
		find.setEnabled(false);
		
		find.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				Location location = locationMngr
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				GeoPoint lastPoint = new GeoPoint((int) (location.getLatitude() * 1E6), (int) (location.getLongitude() * 1E6));
				mapController.setCenter(lastPoint);
				mapController.setZoom(17);
				OverlayItem overlayitem = new OverlayItem(lastPoint,
						"Hi there!", "You are here :)");
				itemizedoverlay.addOverlay(overlayitem);
				mapOverlays.add(itemizedoverlay);
			}
		});

		final Timer myTimer = new Timer(); // create Timer
		myTimer.schedule(new TimerTask() {// create timer task

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if (locationMngr
								.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
							Runnable action = new Runnable() {

								public void run() {
									// TODO Auto-generated method stub
									find.setEnabled(true);
								}
							};
							myTimer.cancel();
							myTimer.purge();
							runOnUiThread(action);
						}
					}
				}, 5000, 3000);

	}
	
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

}
