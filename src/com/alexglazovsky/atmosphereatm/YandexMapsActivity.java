package com.alexglazovsky.atmosphereatm;

import android.os.Bundle;
import ru.yandex.yandexmapkit.MapActivity;
import ru.yandex.yandexmapkit.MapView;

public class YandexMapsActivity extends MapActivity {

	@Override
	public void onCreate(Bundle savedInstaceState) {
		super.onCreate(savedInstaceState);
		setContentView(R.layout.yandex_map);

		MapView mapView = (MapView) findViewById(R.id.map);
		mapView.showZoomButtons(true);
	}

	// ru.yandex.yandexmapkit.MapController yandexMapController =
	// mapView.getMapController();
	// ru.yandex.yandexmapkit.utils.GeoPoint yandexPoint = new
	// ru.yandex.yandexmapkit.utils.GeoPoint(10 * 1E6,15 * 1E6);
	// yandexMapController.setPositionNoAnimationTo(yandexPoint);
}
