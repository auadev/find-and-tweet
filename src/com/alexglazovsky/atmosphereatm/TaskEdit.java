package com.alexglazovsky.atmosphereatm;

import java.sql.Savepoint;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class TaskEdit extends Activity {

	EditText mTitleText;
	EditText mBodyText;
	Long mRowId;
	TasksAdapter mDbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mDbHelper = new TasksAdapter(this);
		mDbHelper.open();
		setContentView(R.layout.task_edit);
		setTitle(R.string.edit_task);

		mTitleText = (EditText) findViewById(R.id.title);
		mBodyText = (EditText) findViewById(R.id.task);
		Button saveButton = (Button) findViewById(R.id.save);
		mRowId = (Long) ((savedInstanceState == null) ? null
				: savedInstanceState.getSerializable(TasksAdapter.KEY_ROWID));
		if (mRowId == null) {
			Bundle extras = getIntent().getExtras();
			mRowId = extras != null ? extras.getLong(TasksAdapter.KEY_ROWID)
					: null;
		}

		populateFields();

		saveButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				setResult(RESULT_OK);
				finish();
			}
		});

	}

	private void populateFields() {
		if (mRowId != null) {
			Cursor task = mDbHelper.fetchNote(mRowId);
			startManagingCursor(task);
			mTitleText.setText(task.getString(task
					.getColumnIndexOrThrow(TasksAdapter.KEY_TITLE)));
			mBodyText.setText(task.getString(task
					.getColumnIndexOrThrow(TasksAdapter.KEY_BODY)));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		saveState();
		outState.putSerializable(TasksAdapter.KEY_ROWID, mRowId);
	}

	private void saveState() {
		String title = mTitleText.getText().toString();
		String body = mBodyText.getText().toString();

		if (mRowId == null) {
			long id = mDbHelper.createNote(title, body);
			if (id > 0) {
				mRowId = id;
			}
		} else {
			mDbHelper.updateNote(mRowId, title, body);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		saveState();
	}

	@Override
	protected void onResume() {
		super.onResume();
		populateFields();
	}
}
