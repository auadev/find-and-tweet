package com.alexglazovsky.atmosphereatm;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;

import com.google.android.maps.OverlayItem;

public class HelloItemizedOverlay extends
		com.google.android.maps.ItemizedOverlay<OverlayItem> {

	ArrayList<OverlayItem> mOvewrlays = new ArrayList<OverlayItem>();

	public HelloItemizedOverlay(Drawable defaultMarker) {
		// TODO Auto-generated constructor stub
		super(boundCenter(defaultMarker));
	}

	public void addOverlay(OverlayItem overlay) {
		mOvewrlays.add(overlay);
		populate();
	}
	
	@Override
	protected OverlayItem createItem(int i) {
		
		return mOvewrlays.get(i);
	}
	
	@Override
	public int size() {
		
		return 1;
	}
	public void clear() {
		mOvewrlays =  new ArrayList<OverlayItem>();
	}
	
	Context mContext;
	public HelloItemizedOverlay(Drawable defaultMarker, Context context) {
		// TODO Auto-generated constructor stub
		super(boundCenterBottom(defaultMarker));
		mContext = context;
	}
	
	@Override
	public boolean onTap(int index) {
		// TODO Auto-generated method stub
		OverlayItem item = mOvewrlays.get(index);
		AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
		dialog.setTitle(item.getTitle());
		dialog.setMessage(item.getSnippet());
		dialog.show();
		return true;
	}
}
