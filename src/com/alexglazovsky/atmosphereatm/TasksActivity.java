package com.alexglazovsky.atmosphereatm;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.SimpleCursorAdapter;

public class TasksActivity extends ListActivity {
	public static final int ACTIVITY_CREATE = 0;
	public static final int ACTIVITY_EDIT = 1;

	public static final int INSERT_ID = Menu.FIRST;
	public static final int DELETE_ID = Menu.FIRST + 1;

	private TasksAdapter mDbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tasks_list);
		mDbHelper = new TasksAdapter(this);
		mDbHelper.open();
		fillData();

		registerForContextMenu(getListView());
	}

	private void fillData() {
		Cursor tasksCursor = mDbHelper.fetchAllNotes();
		startManagingCursor(tasksCursor);

		String[] from = new String[] { TasksAdapter.KEY_TITLE };
		int[] to = new int[] { R.id.text1 };

		SimpleCursorAdapter tasks = new SimpleCursorAdapter(this,
				R.layout.tasks_row, tasksCursor, from, to);
		setListAdapter(tasks);
	}

	void createNote() {
		Intent i = new Intent(this, TaskEdit.class);
		startActivityForResult(i, ACTIVITY_CREATE);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		fillData();
	};

	String ADD_TASK = "Add task";

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(Menu.NONE, INSERT_ID, Menu.NONE, ADD_TASK);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getTitle() == ADD_TASK) {
			createNote();
			return true;
		} else
			return super.onOptionsItemSelected(item);

	}
}
